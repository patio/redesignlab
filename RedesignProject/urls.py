#!/usr/bin/env python
# coding: utf-8
from django.conf.urls import patterns, include, url

from django.contrib import admin
from humidity import urls

urlpatterns = patterns('',
        url(r'^admin/',include(admin.site.urls)),
        url(r'^humidity/',include('humidity.urls')),
        url(r'^',include('django.contrib.staticfiles.urls')),

)
