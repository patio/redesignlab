"""
WSGI config for RedesignProject project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import site
import sys

site.addsitedir("/home/pi/ENV/lib/python2.7/site-packages/")
sys.path.append("/var/www/redesignlab")
sys.path.append("/var/www/redesignlab/RedesignProject")

os.environ.setdefault("DJANGO_SETTING_MODULE","RedesignProject.settings")

activate_env = os.path.expanduser("/home/pi/ENV/bin/activate_this.py")
execfile(activate_env,dict(__file__=activate_env))


from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "RedesignProject.settings")

application = get_wsgi_application()
