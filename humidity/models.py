
# Create your models here.


# Create your models here.
#!/usr/bin/env python
# coding: utf-8
# Create your views here.

from django.db import models
from django.contrib.auth import models as auth_models
from django.utils.translation import ugettext_lazy as _

MAX_LENGTH = 512
MODULE_TREE = type('', (), {})().__module__.split('.')
APP_LABEL = MODULE_TREE[MODULE_TREE.index("models") - 1]


class MxdUser(models.Model):
	nickname = models.CharField(
		_(u'nickname'),
		max_length = MAX_LENGTH,
	)
	password = models.CharField(
		_(u'password'),
		max_length = MAX_LENGTH,
	)

	time_id = models.DateTimeField(
		_(u'time_id'),
		max_length=MAX_LENGTH,
		auto_now_add=True,
		blank=False
	)

	
	login = models.BooleanField( _(u'login'), default=False )

	class Meta:
		app_label = APP_LABEL

	def __unicode__(self):
		return self.nickname

class Environments(models.Model):
	temperature = models.FloatField(u'temperature',blank=True,default=0)
	humitidy = models.FloatField(u'humitidy',blank=True,default=0)
	pressure = models.FloatField(u'pressure',blank=True,default=0)
	time_id = models.DateTimeField(
		_(u'time_id'),
		max_length=MAX_LENGTH,
		auto_now_add=True,
		blank=False
	)

	def __str__(self):
		return self.temperature


		
