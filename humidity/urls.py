from django.conf.urls import patterns,url
from django.conf import settings
from humidity import views
from views import *

urlpatterns = patterns('',
    url(r'^home/', home, name='home'),
    url(r'^registration/', registration, name='registration'),
    url(r'^user/(?P<id>\w+)/$', user, name='user'),
    url(r'^new',new,name='new'),
    url(r'^Reform',Reform,name='Reform'),
    url(r'^exit',exit,name='exit'),
    url(r'^env_data',env_list,name='env_list'),
    url(r'^all_env',all_env,name='all_env'),
    url(r'^check_user_login',check_user_login,name='check_user_login'),
    url(r'^check_user_logout',check_user_logout,name='check_user_logout'),
    url(r'^all_member',all_member,name='all_member'),
    url(r'^logout',logout,name='logout'),

)
