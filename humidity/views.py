#!/usr/bin/python
#conding: utf-8
from django.shortcuts import render
from django.shortcuts import render_to_response,get_object_or_404,redirect
from django.template import RequestContext
from django.http import HttpResponse

 
import json
from collections import OrderedDict

import smbus
import time
 
import datetime
import locale   
d = datetime.datetime.today()
 
bus_number  = 1
i2c_address = 0x76
 
bus = smbus.SMBus(bus_number)
 
digT = []
digP = []
digH = []
 
t_fine = 0.0
timestamp = 0
jikoku = 0
kion = 0
shitsudo = 0
kiatsu = 0

#!/usr/bin/env python
# coding: utf-8
# Create your views here.

from django.shortcuts import render
from django.shortcuts import render_to_response,get_object_or_404,redirect
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from models import *
from datetime import datetime

from django.db.models import Q
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout

from collections import OrderedDict
import json

# Create your views here.

def home(request):
    login_user = MxdUser.objects.filter(login=True)

    return render_to_response('index.html',
        {
            "login_user_num":len(login_user),
        },
        context_instance=RequestContext(request))


def logout(request):
    if request.method == "GET":
        user = MxdUser.objects.get(id=int(request.GET["id"]))
        user.login = False
        user.save()
        
        login_user = MxdUser.objects.filter(login=True)
    return redirect(
        'home',
        )
        

def all_member(request):
    member = MxdUser.objects.all()
    mem = []
    for i in member:
        data =  OrderedDict([ ('nickname',i.nickname)])
        mem.append(data)
    mem_data = OrderedDict([ ('member',mem)])
   
    
    return render_json_response(request, mem_data)      



def render_json_response(request, data, status=None):

    json_str = json.dumps(data, ensure_ascii=False, indent=2)
    callback = request.GET.get('callback') 
    if callback:
        json_str = "%s(%s)" % (callback, json_str)
        response = HttpResponse(json_str, content_type='application/javascript; charset=UTF-8', status=status)
    else:
        response = HttpResponse(json_str, content_type='application/json; charset=UTF-8', status=status)
    return response


def all_env(request):
    obj_env = Environments.objects.all()
    
    return render_to_response('index.html',
        {
          'obj_env':obj_env,
        },
        context_instance=RequestContext(request))

def save_env(request):
    new_env = Environments.objects.create(
        humitidy = env["shitsudo"],
        time_id = datetime.now(),
        temperature = env["kion"],
        pressure = env["kiatsu"],
        )
    new_env.save()
    return render_to_response('index.html',
        {
        },
        context_instance=RequestContext(request))


def env_list(request):
    env_list = []
    env = calc()
    kion = round(env["kion"],2)
    shitsudo = round(env["shitsudo"],2)
    pressure = round(env["kiatsu"],2)
    sensor_data = OrderedDict([ ('temperature',kion),('humidity',shitsudo),('pressure',pressure)])
   
    
    return render_json_response(request, sensor_data)        

@csrf_exempt
def registration(request):
    if request.method == 'POST': 
        nickname = request.POST['User']
        password = request.POST['Pass']
        usrdata = MxdUser.objects.filter(nickname=nickname,password=password)
        if len(usrdata) == 0:
            return redirect(
                'Reform'
            )
        else:
            user = MxdUser.objects.get(nickname=nickname,password=password)
            
            
            return redirect(
                'user',
                id=user.id
                )
    else:
        response = HttpResponse()
        response['msg'] = 'NG'

        return render_to_response(
            'login.html',
            context_instance = RequestContext(request, {}),
        )

def Reform(request):
    return render_to_response(
        'user404.html',
        context_instance = RequestContext(request,{}),
    )
def check_user_logout(request):
    logout_user = MxdUser.objects.filter(login=False)
    users = []
    for user in logout_user:
        user_data = OrderedDict([ ('id',user.id),('nickname',user.nickname)])
        users.append(user_data)

    users_data = OrderedDict([('users',users)])
        
    return render_json_response(request, users_data)           


def check_user_login(request):
    login_user = MxdUser.objects.filter(login=True)
   
    users = []
    for user in login_user:
        user_data = OrderedDict([ ('id',user.id),('nickname',user.nickname)])
        users.append(user_data)

    users_data = OrderedDict([('users',users)])
        
    return render_json_response(request, users_data)            

def user(request,id):
    login_user = MxdUser.objects.filter(login=True)
    logout_user = MxdUser.objects.filter(login=False)
    userdata = MxdUser.objects.get(id=id)
    userdata.login = True
    userdata.save()

    return render_to_response(
        'userpage.html',
        {
            'userdata': userdata,
            'data':calc(),
            'date':d,
            'login_user':login_user,
            'logout_user':logout_user,
            
        },
        context_instance=RequestContext(request))




# Create your views here.
def new(request):
    if request.method == 'POST': 
        nickname = request.POST['User']
        password = request.POST['Pass']
        new_usr = MxdUser.objects.create(
            nickname = nickname,
            time_id = datetime.now(),
            password = password,
            login = True,
            )
        new_usr.save()
        return redirect(
            'user',
            id=new_usr.id
        )
    else:
        user_data = MxdUser.objects.all()
    return render_to_response(
        'new.html',
        {
            'userdata':user_data,
        },
        context_instance=RequestContext(request))

def exit(request):
    if request.method == 'GET':
        usr = MxdUser.objects.get(id=int(request.GET['id']))
        usr.login = False
        usr.save()
    return redirect(
        'home'
        )



def calc():
    data = {"kion":0,"shitsudo":0,"kiatsu":0}

    setup()
    get_calib_param()
    readData()
#csv():      print(d.strftime("%Y%m%d,%H:%M"),",%2.2f,%4.2f,%6.2f" % (kion, shitsudo,kiatsu))
    data["kion"] = round(kion,2)
    data["shitsudo"] = round(shitsudo,2)
    data["kiatsu"] = round(kiatsu,2)

    return data

def writeReg(reg_address, data):
    bus.write_byte_data(i2c_address,reg_address,data)
 
def get_calib_param():
    calib = []
 
    for i in range (0x88,0x88+24):
        calib.append(bus.read_byte_data(i2c_address,i))
    calib.append(bus.read_byte_data(i2c_address,0xA1))
    for i in range (0xE1,0xE1+7):
        calib.append(bus.read_byte_data(i2c_address,i))
 
    digT.append((calib[1] << 8) | calib[0])
    digT.append((calib[3] << 8) | calib[2])
    digT.append((calib[5] << 8) | calib[4])
    digP.append((calib[7] << 8) | calib[6])
    digP.append((calib[9] << 8) | calib[8])
    digP.append((calib[11]<< 8) | calib[10])
    digP.append((calib[13]<< 8) | calib[12])
    digP.append((calib[15]<< 8) | calib[14])
    digP.append((calib[17]<< 8) | calib[16])
    digP.append((calib[19]<< 8) | calib[18])
    digP.append((calib[21]<< 8) | calib[20])
    digP.append((calib[23]<< 8) | calib[22])
    digH.append( calib[24] )
    digH.append((calib[26]<< 8) | calib[25])
    digH.append( calib[27] )
    digH.append((calib[28]<< 4) | (0x0F & calib[29]))
    digH.append((calib[30]<< 4) | ((calib[29] >> 4) & 0x0F))
    digH.append( calib[31] )
 
    for i in range(1,2):
        if digT[i] & 0x8000:
            digT[i] = (-digT[i] ^ 0xFFFF) + 1
 
    for i in range(1,8):
        if digP[i] & 0x8000:
            digP[i] = (-digP[i] ^ 0xFFFF) + 1
 
    for i in range(0,6):
        if digH[i] & 0x8000:
            digH[i] = (-digH[i] ^ 0xFFFF) + 1
 
def readData():
    data = []
    for i in range (0xF7, 0xF7+8):
        data.append(bus.read_byte_data(i2c_address,i))
    pres_raw = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
    temp_raw = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)
    hum_raw  = (data[6] << 8)  |  data[7]
 
    compensate_T(temp_raw)
    compensate_P(pres_raw)
    compensate_H(hum_raw)
 
def compensate_P(adc_P):
    global  t_fine
    pressure = 0.0
 
    v1 = (t_fine / 2.0) - 64000.0
    v2 = (((v1 / 4.0) * (v1 / 4.0)) / 2048) * digP[5]
    v2 = v2 + ((v1 * digP[4]) * 2.0)
    v2 = (v2 / 4.0) + (digP[3] * 65536.0)
    v1 = (((digP[2] * (((v1 / 4.0) * (v1 / 4.0)) / 8192)) / 8)  + ((digP[1] * v1) / 2.0)) / 262144
    v1 = ((32768 + v1) * digP[0]) / 32768
 
    if v1 == 0:
        return 0
    pressure = ((1048576 - adc_P) - (v2 / 4096)) * 3125
    if pressure < 0x80000000:
        pressure = (pressure * 2.0) / v1
    else:
        pressure = (pressure / v1) * 2
    v1 = (digP[8] * (((pressure / 8.0) * (pressure / 8.0)) / 8192.0)) / 4096
    v2 = ((pressure / 4.0) * digP[7]) / 8192.0
    pressure = pressure + ((v1 + v2 + digP[6]) / 16.0)
    global kiatsu
    kiatsu = pressure/100
#       print "pressure : %7.2f hPa" % (pressure/100)
 
def compensate_T(adc_T):
    global t_fine
    v1 = (adc_T / 16384.0 - digT[0] / 1024.0) * digT[1]
    v2 = (adc_T / 131072.0 - digT[0] / 8192.0) * (adc_T / 131072.0 - digT[0] / 8192.0) * digT[2]
    t_fine = v1 + v2
    temperature = t_fine / 5120.0
#       print "\n",
#       print d.strftime("%Y%m%d  %H:%M:%S")
#       print "temp : %-6.2f" % (temperature)
    global kion
    kion = temperature
 
def compensate_H(adc_H):
    global t_fine
    var_h = t_fine - 76800.0
    if var_h != 0:
        var_h = (adc_H - (digH[3] * 64.0 + digH[4]/16384.0 * var_h)) * (digH[1] / 65536.0 * (1.0 + digH[5] / 67108864.0 * var_h * (1.0 + digH[2] / 67108864.0 * var_h)))
    else:
        return 0
    var_h = var_h * (1.0 - digH[0] * var_h / 524288.0)
    if var_h > 100.0:
        var_h = 100.0
    elif var_h < 0.0:
        var_h = 0.0
#      print "hum : %6.2f" % (var_h)
    global shitsudo
    shitsudo = var_h
 
def setup():
    osrs_t = 1                      #Temperature oversampling x 1
    osrs_p = 1                      #Pressure oversampling x 1
    osrs_h = 1                      #Humidity oversampling x 1
    mode   = 3                      #Normal mode
    t_sb   = 5                      #Tstandby 1000ms
    filter = 0                      #Filter off
    spi3w_en = 0                    #3-wire SPI Disable
 
    ctrl_meas_reg = (osrs_t << 5) | (osrs_p << 2) | mode
    config_reg    = (t_sb << 5) | (filter << 2) | spi3w_en
    ctrl_hum_reg  = osrs_h
 
    writeReg(0xF2,ctrl_hum_reg)
    writeReg(0xF4,ctrl_meas_reg)
    writeReg(0xF5,config_reg)
 
