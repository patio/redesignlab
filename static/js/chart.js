var tdata = []; // We'll leave this empty for now.
var ttemp = [];
var ptemp = [];
var htemp = [];
var hdata=[];
var pdata=[];
setInterval(function(){
	$.getJSON("/redesign/humidity/env_data",function(json){
		//console.log(json.temperature);
	for(var i = 60; i >= 0 ;i -= 1){
	    ttemp[i] = ttemp[i-1];
	    ptemp[i] = ptemp[i-1];
	    htemp[i] = htemp[i-1];
	}
	ttemp[1] = json.temperature;
	    htemp[1] = json.humidity;
	    ptemp[1] = json.pressure;
	for(var i = 1; i <= 60; i += 1) {
		tdata.pop();
	    hdata.pop();
	    pdata.pop();
	}
	for(var i = 1; i <= 60; i += 1) {
	    tdata.push({x: i, y: ttemp[i]});
	    hdata.push({x: i, y: htemp[i]});
	    pdata.push({x: i, y: ptemp[i]});
	}
	updata();
	});
},1000);


$.getJSON("/redesign/humidity/env_data",function(json){
for(var i = 1; i <= 60; i += 1) {
	ttemp[i] = json.temperature;   
        htemp[i] = json.humidity;
	 ptemp[i] = json.pressure;
}
	for(var i = 1; i <= 60; i += 1) {
	    tdata.push({x: i, y: ttemp[i]});
	    hdata.push({x: i, y: htemp[i]});
	    pdata.push({x: i, y: ptemp[i]});
	}
	updata();
	
});
/*
for(var i = 1; i <= 60; i += 1) {
	data.push({x: i, y: 0});
}
*/
var data_t = [{
	key: 'y = log(x)',
	values: tdata
}

];
var data_h = [{
    key:'y = log(x)',
    values: hdata
}];
var data_p = [{
    key:'y = log(x)',
    values: pdata

}];

function updata(){
nv.addGraph(function() {
	//Creates a new Line chart.
	var chart = nv.models.lineChart()
	.showLegend(false)
	.showYAxis(true)
	.showXAxis(true);

// Tells NVD3 to display values with two decimal places.
chart.xAxis
	.axisLabel('時間経過（秒）→')

chart.yAxis
	.axisLabel('y')
	.tickFormat(d3.format('.2f'));

// Select the "svg" tag and render the chart.
d3.select('svg#temp')
	.datum(data_t)
	.call(chart);
d3.select('svg#humi')
        .datum(data_h)
        .call(chart);

d3.select('svg#pres')
        .datum(data_p)
        .call(chart);

// This will render the chart every time the
// window is resized, so it will always fit the
// screen.
nv.utils.windowResize(function() {
	chart.update()
});

return chart;

});
};

function updateDraw() {

	// 新しいデータ
	var dataset = get_graph_data(datastore);

	// ドメイン（入力値の範囲）の更新
	xScale.domain(d3.extent(dataset, function(d) { return d.date; }));
	yScale.domain(d3.extent(dataset, function(d) { return d.value; }));

	// アニメーションしますよ、という宣言
	svg = d3.select("#svgchart").transition();

	this.svg.select(".line")   // 折れ線を
		.duration(750) // 750msで
		.attr("d", line(dataset)); // （新しい）datasetに変化させる描画をアニメーション

	this.svg.select(".x.axis") // x軸を
		.duration(750) // 750msで
		.call(xAxis); // （domainの変更によって変化した）xAxisに変化させる描画をアニメーション

	this.svg.select(".y.axis") // y軸を
		.duration(750) // 750msで
		.call(yAxis); // （domainの変更によって変化した）yAxisに変化させる描画をアニメーション
}
