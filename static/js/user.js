$(function() {
	
    var h = $(window).height();
    $(".list").css("height",h+"px");

    var url = window.location.href;
    var url1 = url.split("/");
    var userid = url1[6];


    $(document).ready(function(){
	$(".submit").click(function(){
	    $.get("/redesign/humidity/logout?id="+userid);
	    window.close();
	});
	$(".myid").text(userid);
	var result1 = null;
	var result2 = null;
	
	setInterval(function(){


	    $.getJSON("/redesign/humidity/env_data",function(data){
		var temp = data.temperature.toFixed(2);
		var humi = data.humidity.toFixed(2);
		var pres = data.pressure.toFixed(2);
		$(".temperature p").html("気温：<br>"+temp+"℃");
		$(".humidity p").html("湿度：<br>"+humi+"％");
		$(".pressure p").html("気圧：<br>"+pres+"hPa");
	    });



	    var data ={request:"SEND"};

	    $.when(
		$.getJSON("/redesign/humidity/check_user_login"),
		$.getJSON("/redesign/humidity/check_user_logout")
	    ).done(function(data1,data2){
		if(result1 == null){
		    result1 = data1[0];    
		}else if(result1.users.length != data1[0].users.length){
		    $(".login").html("");
		    for(var i=0;i<data1[0].users.length;i++){
			if(data1[0].users[i].id != parseInt(userid)){
			    $(".login").append("<li class='green'>"+data1[0].users[i].nickname+"<span class='other_state_green'></span></li>");
			}
		    }
		    result1 = data1[0];
		}
		if(result2 == null){
		    result2 = data2[0];    
		}else if(result2.users.length != data2[0].users.length){
		    $(".logout").html("");
		    for(var i=0;i<data2[0].users.length;i++){
			if(data2[0].users[i].id != parseInt(userid)){
			    $(".login").append("<li class='red'>"+data2[0].users[i].nickname+"<span class='other_state_red'></span></li>");
			}
		    }
		    result2 = data2[0];
		}
		
	    }).fail(function(){
		console.log("error");
	    });
	    
	},2000);

	})

})